module github.com/ni

go 1.12

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gocql/gocql v0.0.0-20190610222256-e00e8c6226e8
	github.com/gorilla/mux v1.7.2
)
